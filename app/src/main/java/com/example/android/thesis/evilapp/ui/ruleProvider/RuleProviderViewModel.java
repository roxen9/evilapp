package com.example.android.thesis.evilapp.ui.ruleProvider;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class RuleProviderViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RuleProviderViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue("This is RuleProvider fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}