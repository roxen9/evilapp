package com.example.android.thesis.evilapp.ui.rule23;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.thesis.evilapp.R;

class SmsData {
    private int id;
    private String message;
    private String sender;
    private String receiver;
    private boolean isSender = false;
    private Time timestamp;

    public SmsData() {
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSender(String sender) {
        this.sender = sender;
        this.isSender = true;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
        this.isSender = false;
    }

    public void setTimestamp(Time timestamp) {
        this.timestamp = timestamp;
    }

    private String getDate() {
        return this.timestamp.monthDay + "/" + (this.timestamp.month + 1) + "/" + this.timestamp.year;
    }

    public String toString() {
        String output = "Date: " + this.getDate() + "\n";
        if (this.isSender) {
            output += "Sender: " + this.sender + "\n";
        } else {
            output += "Receiver: " + this.receiver + "\n";
        }
        output += "Message: " + this.message;
        return output;
    }
}

public class Rule23Fragment extends Fragment {

    public static final String INBOX = "content://sms/inbox";
    public static final String SENT = "content://sms/sent";
    private Rule23ViewModel rule23ViewModel;

    private String readInboxSMS() {


        String output = "INBOX SMSes:\n - - - - - - - - - - - - - - - - - - - - - \n";

        try {
            Cursor cursorInbox = getContext().getContentResolver().query(Uri.parse(INBOX), null, null, null, null);
            if (cursorInbox.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsData msgData = new SmsData();
                    for (int idx = 0; idx < cursorInbox.getColumnCount(); idx++) {
                        String column = cursorInbox.getColumnName(idx);
                        if (column.equals("body")) {
                            msgData.setMessage(cursorInbox.getString(idx));
                        } else if (column.equals("address")) {
                            msgData.setSender(cursorInbox.getString(idx));
                        } else if (column.equals("date")) {
                            Time time = new Time();
                            time.set(Long.valueOf(cursorInbox.getString(idx)));
                            msgData.setTimestamp(time);
                        }
                    }
                    // use msgData
                    output += msgData.toString() + "\n - - - - - - - - - - - - - - - - - - - - - \n";
                } while (cursorInbox.moveToNext());
            } else {
                // empty box, no SMS
            }
        } catch (Exception e) {
            Log.wtf("WTF", e.getMessage());
        }

        return output;
    }

    private String readSentSMS() {
        String output = "SENT SMSes:\n - - - - - - - - - - - - - - - - - - - - - \n";

        try {
            Cursor cursorSent = getContext().getContentResolver().query(Uri.parse(SENT), null, null, null, null);
            if (cursorSent.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsData msgData = new SmsData();
                    for (int idx = 0; idx < cursorSent.getColumnCount(); idx++) {
                        String column = cursorSent.getColumnName(idx);
                        if (column.equals("body")) {
                            msgData.setMessage(cursorSent.getString(idx));
                        } else if (column.equals("address")) {
                            msgData.setReceiver(cursorSent.getString(idx));
                        } else if (column.equals("date")) {
                            Time time = new Time();
                            time.set(Long.valueOf(cursorSent.getString(idx)));
                            msgData.setTimestamp(time);
                        }
                    }
                    // use msgData
                    output += msgData.toString() + "\n - - - - - - - - - - - - - - - - - - - - - \n";
                } while (cursorSent.moveToNext());
            } else {
                // empty box, no SMS
            }
        } catch (Exception e) {
            Log.wtf("WTF", e.getMessage());
        }
        return output;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rule23ViewModel =
                ViewModelProviders.of(this).get(Rule23ViewModel.class);
        View root = inflater.inflate(R.layout.fragment_rule23, container, false);
        final TextView outputView = root.findViewById(R.id.rule23_output);

        // Request SMS permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final String permission = Manifest.permission.READ_SMS;
            int hasSpecificPermission = ContextCompat.checkSelfPermission(getContext(), permission);
            if (hasSpecificPermission != PackageManager.PERMISSION_GRANTED
                    && !this.shouldShowRequestPermissionRationale(permission)) {
                this.requestPermissions(new String[]{permission},
                        123);
            }
        }

        Button inboxButton = root.findViewById(R.id.inbox_button_rule23);
        Button sentButton = root.findViewById(R.id.sent_button_rule23);

        Button toggleTextButton = root.findViewById(R.id.toggle_text_button_rule23);
        final TextView descriptionText = root.findViewById(R.id.description_rule23);

        toggleTextButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        int visibility = descriptionText.getVisibility();
                        if (visibility == View.VISIBLE) {
                            descriptionText.setVisibility(View.INVISIBLE);
                            descriptionText.setText("");
                        } else {
                            descriptionText.setVisibility(View.VISIBLE);
                            descriptionText.setText(R.string.rule23_description);
                        }
                    }
                }
        );

        inboxButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        String output = readInboxSMS();
                        outputView.setText(output);
                    }
                }
        );

        sentButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        String output = readSentSMS();
                        outputView.setText(output);
                    }
                }
        );
        return root;
    }
}
