package com.example.android.thesis.evilapp.ui.ruleIntent;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class RuleIntentViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RuleIntentViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}