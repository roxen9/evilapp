package com.example.android.thesis.evilapp.ui.ruleHttp;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class RuleHttpViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RuleHttpViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue("This is RuleHttp fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}