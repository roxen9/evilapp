package com.example.android.thesis.evilapp.ui.home;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.thesis.evilapp.R;
import com.example.android.thesis.evilapp.ui.ruleIntent.RuleIntentViewModel;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RuleIntentViewModel intentViewModel;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = null;

        Activity activity = getActivity();
        Intent intent = activity.getIntent();
        String action = intent.getAction();

        // Intercept intent and display its data
        assert action != null;
        if (action.equals(Intent.ACTION_VIEW)) {
            intentViewModel =
                    ViewModelProviders.of(this).get(RuleIntentViewModel.class);
            root = inflater.inflate(R.layout.fragment_rule_intent, container, false);
            String output = "Intercepted Intent: \n\nAction: " + action + "\n";
            TextView interceptedOutput = (TextView) root.findViewById(R.id.ruleIntent_intercepted);
            Bundle extras = intent.getExtras();

            if (extras != null) {
//                Toast.makeText(getContext(), "We have extras", Toast.LENGTH_LONG).show();
                String[] keys = extras.keySet().toArray(new String[0]);
                for (String key : keys) {
                    output += "- " + key + ": " + extras.getString(key) + "\n";
                }
            } else {
//                Toast.makeText(getContext(), "No extras", Toast.LENGTH_LONG).show();
                output += "No Extras in this Intent";
            }
            interceptedOutput.setText(output);

        } else {
            homeViewModel =
                    ViewModelProviders.of(this).get(HomeViewModel.class);
            root = inflater.inflate(R.layout.fragment_home, container, false);
        }

        return root;
    }

}
