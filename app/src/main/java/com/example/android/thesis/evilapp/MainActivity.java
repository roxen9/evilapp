package com.example.android.thesis.evilapp;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    public static String INTENT_ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_ruleIntent, R.id.nav_ruleProvider, R.id.nav_ruleHttp, R.id.nav_rule8, R.id.nav_rule12, R.id.nav_rule21, R.id.nav_rule23)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        // Request SMS permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final String permission = Manifest.permission.RECEIVE_SMS;
            int hasSpecificPermission = ContextCompat.checkSelfPermission(this, permission);
            if (hasSpecificPermission != PackageManager.PERMISSION_GRANTED
                    && !this.shouldShowRequestPermissionRationale(permission)) {
                this.requestPermissions(new String[]{permission},
                        123);
            }
        }

        // Register SMS_RECEIVED IntentFilter
        final CustomReceiver mReceiver = new CustomReceiver(INTENT_ACTION_SMS_RECEIVED);
        IntentFilter filter = new IntentFilter();
        filter.addAction(INTENT_ACTION_SMS_RECEIVED);

        this.registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        //Unregister the receiver
//        this.unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    class CustomReceiver extends BroadcastReceiver {
        public String intentAction;

        public CustomReceiver(String intentAction) {
            this.intentAction = intentAction;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            TextView senderTextView = ((Activity) context).findViewById(R.id.intercepted_sms_sender);
            TextView messageTextView = ((Activity) context).findViewById(R.id.intercepted_sms_message);

            String intentAction = intent.getAction();

            Log.i("§§§", intentAction);

            if ((intent != null) && intentAction.equals(intentAction)) {
                Bundle bundle = intent.getExtras();
                SmsMessage[] smsMessages;
                String messageFrom = null;
                if (bundle != null) {
                    //PDU = protocol data unit
                    //A PDU is a “protocol data unit”, which is the industry format for an SMS message.
                    //Because SMSMessage reads/writes them you shouldn't need to dissect them.
                    //A large message might be broken into many, which is why it is an array of objects.
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    if (pdus != null) {
                        smsMessages = new SmsMessage[pdus.length];
                        // If the sent message is longer than 160 characters  it will be broken down
                        // in to chunks of 153 characters before being received on the device.
                        // To rectify that receivedMessage is the result of appending every single
                        // short message into one large one for our usage. see:
                        //http://www.textanywhere.net/faq/is-there-a-maximum-sms-message-length

                        for (int i = 0; i < smsMessages.length; i++) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdus[i],
                                        bundle.getString("format"));
                            } else {
                                smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                            }
                            messageFrom = smsMessages[i].getOriginatingAddress();
                        }
                        if (!TextUtils.isEmpty(messageFrom)) {
                            String receivedMessage = "";
                            for (SmsMessage smsMessage : smsMessages) {
                                receivedMessage = receivedMessage + smsMessage.getMessageBody();
                            }
                            Log.i("§§§__FOUND", messageFrom);
                            Log.i("§§§__FOUND", receivedMessage);
                            try {
                                senderTextView.setText("Sender: " + messageFrom);
                                messageTextView.setText("Message: " + receivedMessage);
                            } catch (Exception e) {
                                Log.wtf("WTF", e.getMessage());
                            }


                        } else {
                            Log.i("§§§_NOTHING", "NOTHING FOUND");
                        }
                    }
                }
            }

        }
    }
}
