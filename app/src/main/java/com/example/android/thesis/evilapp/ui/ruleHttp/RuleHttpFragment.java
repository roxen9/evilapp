package com.example.android.thesis.evilapp.ui.ruleHttp;

import android.arch.lifecycle.ViewModelProviders;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.thesis.evilapp.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class RuleHttpFragment extends Fragment {

    private RuleHttpViewModel ruleHttpViewModel;
    public String[] HTTPWEBSITES = new String[]{
            "http://open-up.eu/en", "http://floraofksa.myspecies.info/", "http://go.com/", "http://www.example.com/", "http://www.mit.edu/"
    };

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ruleHttpViewModel =
                ViewModelProviders.of(this).get(RuleHttpViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_rule_http, container, false);

        return root;
    }

}
