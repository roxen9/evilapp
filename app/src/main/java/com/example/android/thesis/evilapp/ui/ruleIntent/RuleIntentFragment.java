package com.example.android.thesis.evilapp.ui.ruleIntent;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.android.thesis.evilapp.R;

public class RuleIntentFragment extends Fragment {

    private RuleIntentViewModel ruleIntentViewModel;

    private void sendIntent() {
        String uri = "https://www.facebook.com";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ruleIntentViewModel =
                ViewModelProviders.of(this).get(RuleIntentViewModel.class);
        View root = inflater.inflate(R.layout.fragment_rule_intent, container, false);

        return root;
    }
}
